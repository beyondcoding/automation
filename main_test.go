package main

import (
	"fmt"
	"net"
	"testing"
	"time"
)

func Test_startListen(t *testing.T) {
	tests := []struct {
		name    string
		port    int
		wantErr bool
	}{
		{
			"Connect on unprivileged port",
			6666,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listener, err := startListen(tt.port)
			if err != nil {
				if !tt.wantErr {
					t.Errorf("startListen() error = %v, wantErr %v", err, tt.wantErr)
				}
				return
			}
			defer listener.Close()
			if tt.wantErr {
				t.Error("Error expected but not received.")
				return
			}
			// Try connecting to the socket
			conn, err := net.DialTimeout("tcp4", net.JoinHostPort("127.0.0.1", fmt.Sprint(tt.port)), 500*time.Millisecond)
			if err != nil {
				t.Errorf("Could not connect to port %v", tt.port)
				return
			}
			conn.Close()
		})
	}
}
