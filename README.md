# Ping Server

This is the amazing *Ping Server* which sends a PONG for every one of your PINGs

## Build

### Requirements

The build has only been tested on a `Ubuntu 20.04 amd64` system.
The following packages need to be installed for building

    golang

The minimum version of the go compiler supported is 1.13.

There is also a dependency to an external library called `go-logging`, which is going to be automatically downloaded during the build.

### Compilation

Start the build using

   go build

This will create a binary called `pingserver` in the source folder.

To create a checksum of the binary run

    sha256sum pingserver > pingserver.sha256sum

## Verify

If you have downloaded the binary, also obtain the checksum file `pingserver.sha256sum` and run

    sha256sum -c pingserver.sha256sum

## Run

Just execute the binary `pingserver` after verification.