package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net"
	"strconv"
	"strings"

	"github.com/op/go-logging"
)

const defaultPort = 6666

func handle(connection net.Conn) {
	defer connection.Close()
	log := logging.MustGetLogger("pingserver")
	lineBytes, incomplete, err := bufio.NewReader(connection).ReadLine()
	if err != nil || incomplete {
		fmt.Printf("Can't read line from client")
		return
	}
	line := string(lineBytes)
	line = strings.ToUpper(line)
	if line == "PING" {
		connection.Write([]byte("PONG"))
	} else {
		log.Warningf("Received bad line: '%v'\n", line)
	}
}

func startListen(port int) (net.Listener, error) {
	listenIP := "0.0.0.0"
	portString := net.JoinHostPort(listenIP, strconv.Itoa(port))
	return net.Listen("tcp", portString)
}

func main() {
	logFile, err := ioutil.TempFile("", "pingserver_log")
	if err != nil {
		fmt.Printf("Can't create log file: %v\n", err)
		return
	}
	defer logFile.Close()
	fmt.Printf("Logging to %v\n", logFile.Name())
	logBackend := logging.NewLogBackend(logFile, "", 0)
	logging.SetBackend(logBackend)

	l, err := startListen(defaultPort)
	if err != nil {
		fmt.Printf("Can't listen on port %v: %v\n", defaultPort, err)
		return
	}
	defer l.Close()
	for {
		connection, err := l.Accept()
		if err != nil {
			fmt.Printf("Can't establish connection: %v\n", err)
			return
		}
		go handle(connection)
	}
}
